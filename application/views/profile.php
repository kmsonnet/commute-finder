<?php 
	if(get_cookie('username') == ''){
		redirect(base_url());
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home | Commute Mate</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/css/w3.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/css/custom.css">
	<script type="text/javascript" src="<?php echo base_url()?>/js/custom.js"></script>		
</head>
<body>
	<div class="w3-center">
	  <div class="w3-row">	   
	  	<a href="<?php echo base_url()?>" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-green w3-margin">HOME</a>	   
	    <a href="javascript:void(0)" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-grey w3-margin w3-blue" onclick="document.getElementById('login').style.display='block'">PROFILE</a>
	    <a href="logout" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-red w3-margin" onclick="document.getElementById('reg').style.display='block'">LOGOUT</a>	 
	  </div>	
	</div>
	<br>
	<br>
	<h1 class="w3-center">Welcome <?php echo get_cookie('username')?></h1>
	<div class="w3-container">
	  <div class="w3-row w3-center">	   	   	  	  
	    	<div class="w3-row w3-center">

		    	<div class="w3-col l4 w3-card-4 w3-padding w3-round">
		    		<h2>ADD COMMUTE</h2>
	  				<hr>
	  				<form action="addcommute" method="post" enctype="multipart/form-data">
	  					<div class="w3-row">
	  						<div class="w3-col l11">
	  							<input type="text" name="origin" class="w3-input w3-border w3-padding w3-round-large" placeholder="Your Origin..." required>
	  						</div>
	  						<div class="w3-col l1">
	  							<span class="w3-text-red">*</span>
	  						</div>	  							
	  					</div>
	  					<br>
	  					<div class="w3-row">
	  						<div class="w3-col l11">
	  							<input type="text" name="dest" class="w3-input w3-border  w3-padding w3-round-large" placeholder="Your Destination..." required>
	  						</div>
	  						<div class="w3-col l1">
	  							<span class="w3-text-red">*</span>
	  						</div>	  							
	  					</div>
	  					<br>
	  					<div class="w3-row">
	  						<div class="w3-col l11">
	  							<input type="text" name="day" class="w3-input w3-border w3-padding w3-round-large" placeholder="Your Travel Day..." required>	 
	  						</div>
	  						<div class="w3-col l1">
	  							<span class="w3-text-red">*</span>
	  						</div>	  							
	  					</div>
	  					<br>
	  					<div class="w3-row">
	  						<div class="w3-col l11">
	  							<input type="time" name="time" class="w3-input w3-border  w3-padding w3-round-large" placeholder="Enter Your Travel Time..." required>	
	  						</div>
	  						<div class="w3-col l1">
	  							<span class="w3-text-red">*</span>
	  						</div>	  							
	  					</div>		
	  					<br>
	  					<div class="w3-row" style="text-align: left;">
	  						<div class="w3-col l11">
	  							
	  							<span>Role:</span>
  								<input class="w3-radio" type="radio" name="role" value="Ride Provider" checked>  						
  								<label>Provide Ride</label>
  								
  								<input class="w3-radio" type="radio" name="role" value="Asking for lift">  							
  								<label>Need a lift</label>
  								
	  						</div>
	  						<div class="w3-col l1" style="text-align: center;">
	  							<span class="w3-text-red">*</span>
	  						</div>	  							
	  					</div>			
	  					<br>	    
	  					<div class="w3-row" style="text-align: left;">
	  						
	  						<input type="checkbox" class="w3-check" onclick="createCarModel(this)"> <span>Add Car Model?</span>
	  						<br>	  						
	  						<div id="carModelCon">	  						
	  						</div>
	  						
	  						<input type="checkbox"  class="w3-check" onclick="createLicense(this)">	<span>Add Driving License?</span> 
	  						<br>	
	  						<div id="licenseCon">	  							
	  						</div>				

	  						<input type="checkbox" class="w3-check" onclick="createInsurance(this)"> <span>Add Car Insurance?</span> 
	  						<br>
	  						<div id="insuranceCon">	  						
	  						</div>		

	  						<input type="checkbox" class="w3-check" onclick="createPrice(this)"> <span>Add Price?</span> 
	  						<br>
	  						<div id="priceCon">	  						
	  						</div>	

	  						<input type="checkbox" class="w3-check" onclick="createImg(this)"> <span>Upload Image?</span> 
	  						<br>
	  						<div id="imgCon">	  							
	  						</div>

	  					</div>	
			    		<button class="w3-button w3-orange w3-large w3-round" style="margin-top:30px">ADD</button>		
		    		</form>
		    	</div>   		


		    	<div class="w3-col l1 w3-padding">
		    		
		    	</div>
	    	    	
	    	    <div class="w3-col l7 w3-card-4 w3-padding-16 w3-round">
		    		<h2>ADDED COMMUTE LIST</h2>
	  				<hr>
	  				<div class="w3-row" style="text-align: center;height: 500px;overflow: scroll;">
	  					<?php foreach ($post as $p) {?>
		  					<div class="w3-col l4">
		  							<?php if($p['link'] != null){?>		  							
		  								<img src="<?php echo base_url().'/img/'.$p['link']?>" style="max-width: 60%;height: 150px">		  				
		  							<?php }else{?>
		  								<img src="<?php echo base_url()?>/img/no.jpg?>" style="max-width: 60%;height: 150px">
		  							<?php }?>
					            <br>
					            <div style="text-align: center;">
					                <span><b>Name :</b><?php echo $p['username']?></span><br>
					                <span><b>Origin:</b> <?php echo $p['origin']?> </span><br>
					                <span><b>Destination: </b> <?php echo $p['destination']?> </span><br>
					                <span><b>Day: </b><?php echo $p['day']?></span>
					                <span><b>Time: </b><?php echo $p['traveltime']?></span>
					            </div>        
					            <br>				            
					            <button class="w3-button w3-green w3-small w3-round" onclick="document.getElementById('<?php echo 'edit'.$p['pid']?>').style.display='block'" >Edit</button>
					            <button class="w3-button w3-teal w3-small w3-round"  onclick="document.getElementById('<?php echo $p['pid']?>').style.display='block'">Show</button>
					            <form action="delete" method="post">
					            	<input type="text" name="upPid" class="w3-input w3-hide" value="<?php echo $p['pid']?>"><br>
					            	<button class="w3-button w3-red w3-small w3-round">Delete</button>		
					            </form>
					           		            
		  					</div>


		  					<!-- Start edit modal -->
		  					<div id="<?php echo 'edit'.$p['pid']?>" class="w3-modal ">
							 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
							      <header class="w3-container w3-teal"> 
							        <span onclick="document.getElementById('<?php echo 'edit'.$p['pid']?>').style.display='none'" 
							        class="w3-button w3-display-topright">&times;</span>
							        <h2 class="w3-center">Edit Commute</h2>
							      </header>
							      <div class="w3-container w3-padding-32 w3-center">
							       	<?php if($p['link'] != null){?>		  							
		  								<img src="<?php echo base_url().'/img/'.$p['link']?>" style="max-width: 60%">		  				
		  							<?php }else{?>
		  								<img src="<?php echo base_url()?>/img/no.jpg?>">
		  							<?php }?>
		  							<br>
		  							<br>
		  							<div style="text-align: center;">

		  								<form action="update" method="post">
		  									<input type="text" name="upUsername" class="w3-input w3-hide" value="<?php echo $p['username']?>"><br>
		  									<input type="text" name="upPid" class="w3-input w3-hide" value="<?php echo $p['pid']?>"><br>
		  									<input type="text" name="origin" class="w3-input w3-border" placeholder="Enter Origin" value="<?php echo $p['origin']?>"><br>
		  									<input type="text" name="des" class="w3-input w3-border" placeholder="Enter Destination" value="<?php echo $p['destination']?>"><br>
		  									<input type="text" name="day" class="w3-input w3-border" placeholder="Enter Day" value="<?php echo $p['day']?>"><br>
		  									<input type="time" name="time" class="w3-input w3-border" placeholder="Enter Time" value="<?php echo $p['traveltime']?>"><br>
		  									
		  									<h3>Additional Detials</h3><br>
		  									<input type="text" name="price" class="w3-input w3-border" placeholder="Enter Fare Rate" value="<?php echo $p['amount']?>"><br>
		  									<input type="text" name="model" class="w3-input w3-border" placeholder="Enter Car Model" value="<?php echo $p['model']?>"><br>
		  									<div class="w3-col l11">
	  							
					  							<span>Role:</span>
				  								<input class="w3-radio" type="radio" name="role" value="Ride Provider" checked>  						
				  								<label>Provide Ride</label>
				  								
				  								<input class="w3-radio" type="radio" name="role" value="Asking for lift">  							
				  								<label>Need a lift</label>
				  								
					  						</div>
					  						<br>
					  						<br>
		  									<input type="text" name="license" class="w3-input w3-border" placeholder="Enter License" value="<?php echo $p['state']?>"><br>
		  									<input type="text" name="insurance" class="w3-input w3-border" placeholder="Enter Insurance" value="<?php echo $p['instate']?>"><br>

		  									<button class="w3-button w3-orange">Update</button>
		  								</form>						               						            						             
					        	    </div>  
							      </div>
							      <footer class="w3-container w3-teal w3-center w3-padding-16">	        
							      </footer>
						   		 </div>
						 	 </div>
						 	 <!-- End edit modal -->

		  					<!-- Start show modal -->
		  					<div id="<?php echo $p['pid']?>" class="w3-modal ">
							 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
							      <header class="w3-container w3-teal"> 
							        <span onclick="document.getElementById('<?php echo $p['pid']?>').style.display='none'" 
							        class="w3-button w3-display-topright">&times;</span>
							        <h2 class="w3-center">Commute Details</h2>
							      </header>
							      <div class="w3-container w3-padding-32 w3-center">
							       	<?php if($p['link'] != null){?>		  							
		  								<img src="<?php echo base_url().'/img/'.$p['link']?>" style="max-width: 60%">		  				
		  							<?php }else{?>
		  								<img src="<?php echo base_url()?>/img/no.jpg?>">
		  							<?php }?>
		  							<br>
		  							<div style="text-align: center;">
						                <span><b>Name :</b><?php echo $p['username']?></span><br>
						                <span><b>Origin:</b> <?php echo $p['origin']?> </span><br>
						                <span><b>Destination: </b> <?php echo $p['destination']?> </span><br>
						                <span><b>Day: </b><?php echo $p['day']?></span>
						                <span><b>Time: </b><?php echo $p['traveltime']?></span>
						                <h3 class="w3-border-bottom">Additional Detials</h3>
						                <span><b>Fare Rate: </b>$<?php echo $p['amount']?>/km</span><br>				                
						                <span><b>Car Model: </b><?php echo $p['model']?></span><br>
						                <span><b>Role: </b><?php echo $p['role']?></span><br>						            
						               	<span><b>Driving License: </b><?php echo $p['state']?></span><br>						             <span><b>Insurance: </b><?php echo $p['instate']?></span><br>
					        	    </div>  
							      </div>
							      <footer class="w3-container w3-teal w3-center w3-padding-16">	        
							      </footer>
						   		 </div>
						 	 </div>
						 	 <!-- End show modal -->

	  					<?php }?>	  							  						  			
	  				</div>
		    	</div>


	    	</div>	    		
	  </div>
	</div>	

</body>
</html>