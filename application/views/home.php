<!DOCTYPE html>
<html>
<head>
	<title>Home | Commute Mate</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/css/w3.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/css/custom.css">
	<script type="text/javascript" src="<?php echo base_url()?>/js/custom.js"></script>
	<style type="text/css">
		body{
			background-image: url("<?php echo base_url()?>/img/back1.jpg");
			background-attachment: fixed;
		    background-position: center;
		    background-repeat: no-repeat;
		    background-size: cover;
		}
	</style>
</head>
<body onload="showCaptcha()">
	<div class="w3-center">
	  <div class="w3-row">	   
	  	<a href="javascript:void(0)" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-green w3-margin">HOME</a>
	  	<?php if(get_cookie('username') == ''){?>
			<a href="javascript:void(0)" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-blue w3-margin" onclick="document.getElementById('login').style.display='block'">LOGIN</a>
	   		<a href="javascript:void(0)" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-red w3-margin" onclick="document.getElementById('reg').style.display='block'">REGISTER</a>	 
		<?php } else{?>
		  	<a href="profile" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-grey w3-margin w3-blue" >PROFILE</a>
		    <a href="logout" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-red w3-margin">LOGOUT</a>	   
	   	<?php } ?>
	  </div>	 
	</div>

	<p class="w3-text-red w3-xlarge"><?php echo $regErr?></p>
	<p class="w3-text-red w3-xlarge w3-center"><?php echo $logErr?></p>

	<div class="w3-container" style="margin:10% auto">
	  <div class="w3-row w3-center">	   	   
	  	<h1>Find Your Commuter</h1>
	  	<br>
	  	
	    <form action="search" method="post">
	    	<div class="w3-row-padding w3-center">
	    		<div class="w3-col l4">
	    			<input type="text" name="origin" class="w3-input w3-border w3-padding-16 w3-round-large" placeholder="Your Origin..." required>
	    		</div>
	    		<div class="w3-col l4">	    			
	    			<input type="text" name="dest" class="w3-input w3-border  w3-padding-16 w3-round-large" placeholder="Your Destination..." required>
	    		</div>
	    		<div class="w3-col l2">	    			
	    			<input type="text" name="day" class="w3-input w3-border w3-padding-16 w3-round-large" placeholder="Your Travel Day..." required>
	    		</div>
	    		<div class="w3-col l2">	    			
	    			<input type="time" name="time" class="w3-input w3-border  w3-padding-16 w3-round-large" placeholder="Enter Your Travel Time..." required>
	    		</div>
	    		<div  >
	    			<button class="w3-button w3-orange w3-large w3-round" style="margin-top:30px">Search</button>	
	    		</div>
	    		
	    	</div>
	    	
	    </form>
	  </div>
	</div>

	<div id="login" class="w3-modal ">
	 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
	      <header class="w3-container w3-teal"> 
	        <span onclick="document.getElementById('login').style.display='none'" 
	        class="w3-button w3-display-topright">&times;</span>
	        <h2 class="w3-center">Login</h2>
	      </header>
	      <div class="w3-container w3-padding-32 w3-center ">
	       	<form action="login" method="post">
	       		<input type="text" name="username1" placeholder="Enter username ..." class="w3-input w3-border">
	       		<br>
	       		<input type="password" name="password" placeholder="Enter password ..." class="w3-input w3-border">
	       		<br>
	       		<button class="w3-btn w3-green w3-round-xlarge">LOGIN</button>
	       	</form>
	      </div>
	      <footer class="w3-container w3-teal w3-center w3-padding-16">
	        <a href="#"  onclick="showAndHide('reg', 'login')" style="text-decoration: none">Create new account?</a>	        	       
	      </footer>
    	</div>
 	 </div>

 	  <div id="reg" class="w3-modal ">
	 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
	      <header class="w3-container w3-teal"> 
	        <span onclick="document.getElementById('reg').style.display='none'" 
	        class="w3-button w3-display-topright">&times;</span>
	        <h2 class="w3-center">Join</h2>
	      </header>
	      <div class="w3-container w3-padding-32 w3-center">
	       	<form action="reg" method="post">	      
	       		<input type="text" name="username" placeholder="Choose an username ..." class="w3-input w3-border" onkeyup="showHint(this.value)" required>
	       		<p class="w3-text-red" id="usernameErr"></p>
	       		<br>	       	      
	       		<input type="password" name="password" placeholder="Choose a Password ..." class="w3-input w3-border" required="">
	       		<br>	       		
	       		<input type="email" name="email" placeholder="Enter a Valid Email ..." class="w3-input w3-border" required="">
	       		<br>	       	
	       		<div style="display: inline;" id='captcha'>
	       						
	       		</div>		    	       		  
	       		<a href="javascript:void(0)" style="display: inline;" onclick="showCaptcha()"><img src="<?php echo base_url()?>/img/cap/re.png" style="max-width: 40px"></a>
	       		<br>
	       		<p id="captchaErr" class="w3-text-red"></p>
	       		<br>
	       		<input class="w3-btn w3-green w3-round-large" type="submit" id="submitBtn" value="JOIN">

	       	</form>
	      </div>
	      <footer class="w3-container w3-teal w3-center w3-padding-16">	        
	      </footer>
    	</div>
 	 </div>

</body>
</html>