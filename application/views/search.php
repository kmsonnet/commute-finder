<!DOCTYPE html>
<html>
<head>
	<title>Home | Commute Mate</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/css/w3.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/css/custom.css">
	<script type="text/javascript" src="<?php echo base_url()?>/js/custom.js"></script>
	
</head>
<body>
	<div class="w3-center">
	  <div class="w3-row">	   
	  	<a href="<?php echo base_url()?>" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-green w3-margin">HOME</a>	   
	   	<?php if(get_cookie('username') == ''){?>
			<a href="javascript:void(0)" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-blue w3-margin" onclick="document.getElementById('login').style.display='block'">LOGIN</a>
	   		<a href="javascript:void(0)" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-red w3-margin" onclick="document.getElementById('reg').style.display='block'">REGISTER</a>	 
		<?php } else{?>
		  	<a href="<?php echo base_url()?>/profile" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-grey w3-margin w3-blue" >PROFILE</a>
		    <a href="<?php echo base_url()?>/logout" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-red w3-margin">LOGOUT</a>	   
	   	<?php } ?> 
	  </div>	 
	</div>

	<div class="w3-container" style="margin:1% auto">
	  <div class="w3-row w3-center">	   	   
		<br>	  	
		<?php
			if($pagesize%6==0){
				$pagesize = $pagesize/6;				
			}
			else{
				$pagesize = (int)($pagesize/6)+1;				
			}
		?>
	    <form action="<?php echo base_url()?>search" method="post">
	    	<div class="w3-row-padding w3-center">
	    		<div class="w3-col l4">
	    			<input type="text" name="origin" class="w3-input w3-border w3-padding w3-round-large" value="<?php echo  get_cookie('origin')?>" placeholder="Your Origin..." required>
	    		</div>
	    		<div class="w3-col l4">	    			
	    			<input type="text" name="dest" class="w3-input w3-border  w3-padding w3-round-large" value="<?php echo  get_cookie('des')?>" placeholder="Your Destination..." required>
	    		</div>
	    		<div class="w3-col l2">	    			
	    			<input type="text" name="day" class="w3-input w3-border w3-padding w3-round-large" value="<?php echo  get_cookie('day')?>" placeholder="Your Travel Day..." required>
	    		</div>
	    		<div class="w3-col l2">	    			
	    			<input type="time" name="time" class="w3-input w3-border  w3-padding w3-round-large" value="<?php echo  get_cookie('time')?>" placeholder="Enter Your Travel Time..." required>
	    		</div>
	    		<div>
	    			<button class="w3-button w3-orange w3-large w3-round" style="margin-top:30px">Search Again</button>	
	    		</div>	    		
	    	</div>	    	
	    </form>
	  </div>
	</div>
	<br>

		  			<div class="w3-row w3-padding" style="text-align: center;">
		  				<?php foreach($result as $r) {?>
		  					<div class="w3-col l2">
		  						<img src="<?php echo base_url()?>/img/no.jpg" style="max-width: 60%">
					            <br>
					            <div style="text-align: center;">					               					             
					                <span><b>Origin:</b> <?php echo $r['origin']?></span><br>
					                <span><b>Destination:</b> <?php echo $r['destination']?></span><br>				              
					            </div>        
					            <br>				 
					            <?php if(get_cookie('username') == '') { ?>
					            	<button class="w3-button w3-teal w3-small w3-round" onclick="document.getElementById('login').style.display='block'">Show details</button>									
								<?php } else{?>           				          					           
					             <button class="w3-button w3-teal w3-small w3-round" onclick="document.getElementById('<?php echo $r['pid']?>').style.display='block'">Show Details</button>
					            <?php }?>			          			            
		  					</div>

		  					<!-- Start show modal -->
		  					<div id="<?php echo $r['pid']?>" class="w3-modal ">
							 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
							      <header class="w3-container w3-teal"> 
							        <span onclick="document.getElementById('<?php echo $r['pid']?>').style.display='none'" 
							        class="w3-button w3-display-topright">&times;</span>
							        <h2 class="w3-center">Commute Details</h2>
							      </header>
							      <div class="w3-container w3-padding-32 w3-center">
							       	<?php if($r['link'] != null){?>		  							
		  								<img src="<?php echo base_url().'/img/'.$r['link']?>" style="max-width: 60%">		  				
		  							<?php }else{?>
		  								<img src="<?php echo base_url()?>/img/no.jpg?>">
		  							<?php }?>
		  							<br>
		  							<div style="text-align: center;">
						                <span><b>Userame :</b><?php echo $r['username']?></span><br>
						                <span><b>Origin:</b> <?php echo $r['origin']?> </span><br>
						                <span><b>Destination: </b> <?php echo $r['destination']?> </span><br>
						                <span><b>Day: </b><?php echo $r['day']?></span>
						                <span><b>Time: </b><?php echo $r['traveltime']?></span>
						                <h3 class="w3-border-bottom">Additional Detials</h3>
						                <span><b>Fare Rate: </b>$<?php echo $r['amount']?>/km</span><br>				                
						                <span><b>Car Model: </b><?php echo $r['model']?></span><br>
						                <span><b>Role: </b><?php echo $r['role']?></span><br>						            
						               	<span><b>Driving License: </b><?php echo $r['state']?></span><br>						             <span><b>Insurance: </b><?php echo $r['instate']?></span><br>
					        	    </div>  
							      </div>
							      <footer class="w3-container w3-teal w3-center w3-padding-16">	        
							      </footer>
						   		 </div>
						 	 </div>
		  				<?php } ?> 	  					
	  				</div>
	  				<br>
	  				<div class="w3-row">
	  					<div class="w3-bar w3-center">
	  							<?php for($i=0;$i<$pagesize;$i++){?>	  						
	  						  	  <a href="<?php echo base_url().'search/'.($i+1)?>" class="w3-button w3-border"><?php echo $i+1?></a>
								<?php }?>
						</div>
	  				</div>
					<div id="login" class="w3-modal ">
				 	 <div class="w3-modal-content  w3-round-xxlarge" style="width: 500px">
				      <header class="w3-container w3-teal"> 
				        <span onclick="document.getElementById('login').style.display='none'" 
				        class="w3-button w3-display-topright">&times;</span>
				        <h2 class="w3-center">Login to See Details</h2>
				      </header>
				      <div class="w3-container w3-padding-32 w3-center ">
				       	<form action="login" method="post">
				       		<input type="text" name="username1" placeholder="Enter username ..." class="w3-input w3-border">
				       		<br>
				       		<input type="password" name="password" placeholder="Enter password ..." class="w3-input w3-border">
				       		<br>
				       		<button class="w3-btn w3-green w3-round-xlarge">LOGIN</button>
				       	</form>
				      </div>
				      <footer class="w3-container w3-teal w3-center w3-padding-16">
				        <a href="#"  onclick="showAndHide('reg', 'login')" style="text-decoration: none">Create new account?</a>	        	       
				      </footer>
			    	</div>
			 	 </div>



 
</body>
</html>