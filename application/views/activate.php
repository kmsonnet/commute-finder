<?php 
	if(get_cookie('username') == ''){
		redirect(base_url());
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home | Commute Mate</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/css/w3.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/css/custom.css">
	<script type="text/javascript" src="<?php echo base_url()?>/js/custom.js"></script>		
</head>
<body>
	<div class="w3-center">
	  <div class="w3-row">	   
	  	<a href="<?php echo base_url()?>" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-green w3-margin">HOME</a>	   
	    <a href="javascript:void(0)" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-grey w3-margin w3-blue" onclick="document.getElementById('login').style.display='block'">PROFILE</a>
	    <a href="logout" class="w3-bar-item w3-button w3-xlarge  w3-bottombar w3-border-red w3-margin" onclick="document.getElementById('reg').style.display='block'">LOGOUT</a>	 
	  </div>	
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<p>
		<!-- <?php echo get_cookie('username')?> -->
	</p>
	<div class="w3-container" style="max-width: 500px;margin: 0px auto">
	  <div class="w3-row w3-center">	   	   	  	  
	    	<form action="activateuser" method="post">
	    		<p>Check your Mail. We've Sent you an Activation Code</p>
	    		<input type="text" name="activate" class="w3-input w3-border" placeholder="Enter Activation Code">
	    		<br>
	    		<button class="w3-button w3-green">Activate</button>
	    	</form>
	  </div>
	</div>	

</body>
</html>