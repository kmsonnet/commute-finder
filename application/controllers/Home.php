<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();   
        $this->load->model('Insert_Data_Model');       
    }

	public function index()
	{
		$data['regErr'] = '';
		$data['logErr'] = '';
		
		$this->load->view('home', $data);
	}

	public function profile(){

		$data['post'] = $this->Insert_Data_Model->getData(get_cookie('username'));
		$this->load->view('profile', $data);
	}

	public function defaultSearch(){
		$username = get_cookie('username');
		$origin = $this->input->post('origin');
		$des =  $this->input->post('dest');
		$day =  $this->input->post('day');
		$time =  $this->input->post('time');

		$day = substr($day, 0, 3);

		set_cookie('origin',$origin,'36000'); 
		set_cookie('des',$des,'36000'); 
		set_cookie('time',$time,'36000'); 
		set_cookie('day',$day,'36000'); 

		$data['result']=$this->Insert_Data_Model->dSearch($origin,$des,$day,$time);
		$data['pagesize'] =$this->Insert_Data_Model->rowNumber($origin,$des,$day,$time);
		$this->load->view('search', $data);
	}

	public function pagination(){
		$origin = get_cookie('origin');
		$des =  get_cookie('dest');
		$day =  get_cookie('day');
		$time =  get_cookie('time');		
		$data['result']=$this->Insert_Data_Model->pSearch($origin,$des,$day,$time);
		$data['pagesize'] =$this->Insert_Data_Model->rowNumber($origin,$des,$day,$time);
		$this->load->view('search', $data);
		
	}

	public function activate(){
		$this->load->view('activate');
	}

	public function reg(){
		$code = substr(md5(microtime()),rand(0,26),5);
		if($this->Insert_Data_Model->storeRegData($code)){
			$username = $this->input->post('username');
			set_cookie('username',$username,'3600'); 	

			$to = $this->input->post('email');
			$rcver = $this->input->post('name');
			$this->email->from('ssonnet36@gmail.com', 'Sajid');
			$this->email->to($to); 		
			$this->email->subject('Welcome');
			$this->email->message("Hello"." ".$rcver." ". "Welcome To our Site. Your Activation Code is ".$code);	
			$this->email->send();					
			redirect('activate');
		}
		else{
			$data['regErr'] = "Something Goes Wrong! Please Try Again";
			$this->load->view('home', $data);
		}

	}

	public function login(){
		if($this->Insert_Data_Model->checkLogin()){			
			$username = $this->input->post('username1');
			set_cookie('username',$username,'3600'); 

			if($this->Insert_Data_Model->checkActivation()){
				redirect('activate');
			}else{
				redirect('profile');
			}
		}else{
			$data['logErr'] = "Username or Password is incorrect";
			$data['regErr'] = '';
			$this->load->view('home', $data);
		}
	}

	public function activateUser(){
		if($this->Insert_Data_Model->deleteActivateUser(get_cookie('username'))){
			redirect('profile');
		}else{
			$data['codeErr'] = "Invalid Code";
			$this->load->view('activate', $data);
		}

	}

	public function addCommute(){
		$username = get_cookie('username');
		$pid = substr(md5(microtime()),rand(0,26),10);

		if($this->Insert_Data_Model->insertDeafaultPost($username, $pid)){
			$this->Insert_Data_Model->insertAdditionalPostData($pid);
			redirect('profile');
		}

	}

	public function logout(){
		delete_cookie('username');
		redirect(base_url());
	}

	public function update(){
		$this->Insert_Data_Model->updateData();
		redirect('profile');
	}

	public function delete(){
		$this->Insert_Data_Model->deleteData();
		redirect('profile');
	}
		
}
