<?php
class Insert_Data_Model extends CI_Model {

	public function storeRegData($code){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email 	  = $this->input->post('email');

		$password = md5($password);

		$sqlcode ="INSERT INTO inactive (username, code) VALUES ('$username', '$code')";	
		$sqldata = "INSERT INTO user_info (username, password, email) VALUES ('$username', '$password', '$email')";

		if($this->db->query($sqlcode) && $this->db->query($sqldata)){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function checkLogin(){
		$username = $this->input->post('username1');
		$password = $this->input->post('password');		
		$password = md5($password);

		$sql = "SELECT * FROM user_info WHERE username ='$username' AND password = '$password'";
		if(($this->db->query($sql)->num_rows())>0){
			return true;
		}else{
			return false;
		}
	}

	public function checkActivation(){
		$username = $this->input->post('username1');

		$sql = "SELECT * FROM inactive WHERE username ='$username'";
		if(($this->db->query($sql)->num_rows())>0){
			return true;
		}else{
			return false;
		}
	}

	public function deleteActivateUser($username){
		$code = $this->input->post("activate");
		$sql = "DELETE FROM inactive WHERE code = '$code'";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	public function insertDeafaultPost($username, $pid){
		$origin = $this->input->post('origin');
		$des =  $this->input->post('dest');
		$day =  $this->input->post('day');
		$time =  $this->input->post('time');
		$role = $this->input->post('role');

		$sql = "INSERT INTO post (username, origin, destination, traveltime, day, role, pid) VALUES ('$username', '$origin', '$des', '$time', '$day', '$role', '$pid')";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	public function insertAdditionalPostData($pid){
		$carModel = $this->input->post('carModel');
		$license = $this->input->post('license');
		$insurance = $this->input->post('insurance');
		$price = $this->input->post('price');
		$img = $this->input->post('img');

		if($carModel != null){
			$sql = "INSERT INTO car_model (pid, model) VALUES ('$pid', '$carModel')";
			$this->db->query($sql);	
		}else{
			$sql = "INSERT INTO car_model (pid) VALUES ('$pid')";
			$this->db->query($sql);	
		}

		if($license != null){
			$sql = "INSERT INTO licensed (pid, state) VALUES ('$pid','$license')";
			$this->db->query($sql);		
		}else{
			$sql = "INSERT INTO licensed (pid) VALUES ('$pid')";
			$this->db->query($sql);	
		}	

		if($insurance != null){
			$sql = "INSERT INTO insurance (pid, instate) VALUES ('$pid', '$insurance')";
			$this->db->query($sql);		
		}else{
			$sql = "INSERT INTO insurance (pid) VALUES ('$pid')";
			$this->db->query($sql);	
		}

		if($price != null){
			$sql = "INSERT INTO price (pid, amount) VALUES ('$pid', '$price')";
			$this->db->query($sql);		
		}else{
			$sql = "INSERT INTO price (pid) VALUES ('$pid')";
			$this->db->query($sql);	
		}

		if(!empty($_FILES['pic']['name'])){
			 $config['upload_path'] ='img/';
             $config['allowed_types'] = 'jpg|jpeg|png|gif';
             $config['file_name'] = $_FILES['pic']['name'];

            $this->load->library('upload',$config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('pic')){
                $uploadData = $this->upload->data();
                $picture = $uploadData['file_name'];  
                $sql = "INSERT INTO img (pid, link) VALUES ('$pid', '$picture')";
				$this->db->query($sql);		            
            }
		}else{
			$sql = "INSERT INTO img (pid, link) VALUES ('$pid', '')";
			$this->db->query($sql);	
		}
	}

	function getData($username){
		$sql = "SELECT  u.pid, u.username, u.origin, u.destination, u.traveltime, u.day, u.role, u.traveltime, c.model, i.link, ins.instate, l.state, p.amount FROM post u
			INNER JOIN car_model c ON u.pid = c.pid
			INNER JOIN img i ON u.pid = i.pid
			INNER JOIN insurance ins ON u.pid = ins.pid
 			INNER JOIN licensed l ON u.pid = l.pid
			INNER JOIN price p ON u.pid = p.pid WHERE u.username = '$username'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function dSearch($origin, $des, $day, $time){
			
			$sql = "SELECT  u.pid, u.username, u.origin, u.destination, u.traveltime, u.day, u.role, u.traveltime, c.model, i.link, ins.instate, l.state, p.amount FROM post u
			LEFT JOIN car_model c ON u.pid = c.pid
			LEFT  JOIN img i ON u.pid = i.pid
			LEFT  JOIN insurance ins ON u.pid = ins.pid
 			LEFT JOIN licensed l ON u.pid = l.pid
			LEFT  JOIN price p ON u.pid = p.pid WHERE u.origin = '$origin' AND u.destination = '$des' AND u.traveltime = '$time' AND u.day = '$day' LIMIT 6 OFFSET 0";

			$query = $this->db->query($sql);
			return $query->result_array();
	}

	function pSearch($origin, $des, $day, $time){

			$pagenumber =  $this->uri->segment(2);
			$offset = ($pagenumber*6)-6;
			$sql = "SELECT  u.pid, u.username, u.origin, u.destination, u.traveltime, u.day, u.role, u.traveltime, c.model, i.link, ins.instate, l.state, p.amount FROM post u
			LEFT JOIN car_model c ON u.pid = c.pid
			LEFT  JOIN img i ON u.pid = i.pid
			LEFT  JOIN insurance ins ON u.pid = ins.pid
 			LEFT JOIN licensed l ON u.pid = l.pid
			LEFT  JOIN price p ON u.pid = p.pid WHERE u.origin = '$origin' AND u.destination = '$des' AND u.traveltime = '$time' AND u.day = '$day' LIMIT $offset";

			$query = $this->db->query($sql);
			return $query->result_array();
	}

	function rowNumber($origin, $des, $day, $time){
		$sql = "SELECT  u.pid, u.username, u.origin, u.destination, u.traveltime, u.day, u.role, u.traveltime, c.model, i.link, ins.instate, l.state, p.amount FROM post u
			LEFT JOIN car_model c ON u.pid = c.pid
			LEFT JOIN img i ON u.pid = i.pid
			LEFT JOIN insurance ins ON u.pid = ins.pid
 			LEFT JOIN licensed l ON u.pid = l.pid
			LEFT JOIN price p ON u.pid = p.pid WHERE u.origin = '$origin' AND u.destination = '$des' AND u.traveltime = '$time' AND u.day = '$day'";

		return $this->db->query($sql)->num_rows();
	}

	function updateData(){
		$origin = $this->input->post('origin');
		$des =  $this->input->post('des');
		$day =  $this->input->post('day');
		$time =  $this->input->post('time');
		$role = $this->input->post('role');

		$carModel = $this->input->post('model');
		$license = $this->input->post('license');
		$insurance = $this->input->post('insurance');
		$price = $this->input->post('price');

		$username = $this->input->post('upUsername');
		$pid = $this->input->post('upPid');

		$sql = "UPDATE post SET origin ='$origin', destination = '$des', traveltime = '$time', day ='$day', role = '$role' WHERE pid = 
		'$pid'";		
		$query = $this->db->query($sql);

		$sql = "UPDATE price SET amount = '$price' WHERE pid = '$pid'";
		$query = $this->db->query($sql);

		$sql = "UPDATE insurance SET instate ='$insurance' WHERE pid = '$pid'";
		$query = $this->db->query($sql);

		$sql = "UPDATE licensed SET state = '$license' WHERE pid = '$pid'";
		$query = $this->db->query($sql);

		$sql = "UPDATE car_model SET model='$carModel' WHERE pid = '$pid'";
		$query = $this->db->query($sql);
	}

	public function deleteData(){
		$pid = $this->input->post('upPid');
		$sql = "DELETE FROM post WHERE pid = '$pid'";
		$query = $this->db->query($sql);

		$sql = "DELETE FROM price WHERE pid = '$pid'";
		$query = $this->db->query($sql);

		$sql = "DELETE FROM insurance WHERE pid = '$pid'";
		$query = $this->db->query($sql);

		$sql = "DELETE FROM licensed WHERE pid = '$pid'";
		$query = $this->db->query($sql);

		$sql = "DELETE FROM car_model WHERE pid = '$pid'";
		$query = $this->db->query($sql);

	}
}