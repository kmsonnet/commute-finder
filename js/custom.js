function showAndHide(show, hide){
    document.getElementById(show).style.display = "block";
    document.getElementById(hide).style.display = "none";
}


function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("usernameErr").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("usernameErr").innerHTML = this.responseText;
                var x = document.getElementById('usernameErr');
                var z = document.getElementById('captchaErr');
                var y = document.getElementById('submitBtn');
                if(x.innerHTML == "" && z.innerHTML == ""){
					y.disabled = false;
				}else{
					y.disabled = true;
				}
            }
        };
        xmlhttp.open("GET", "js/checkUsername.php?q=" + str, true);
        xmlhttp.send();
    }
}

function showCaptcha() {
   
        var min = 1;
        var max = 3;
        
        var id = Math.floor(Math.random() * (max - min + 1)) + min;        
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("captcha").innerHTML = this.responseText;               
            }
        };
        xmlhttp.open("GET", "js/captcha.php?q=" + id, true);
        xmlhttp.send();
}

function checkCaptcha(){
        var value = document.getElementById("uservalue").value;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
               
                document.getElementById("captchaErr").innerHTML = this.responseText;  
                var x = document.getElementById('captchaErr');
                var z = document.getElementById('usernameErr');
                var y = document.getElementById('submitBtn');
                if(x.innerHTML == "" && z.innerHTML ==""){
                    y.disabled = false;
                }else{
                    y.disabled = true;
                }             
            }
        };
        xmlhttp.open("GET", "js/checkCaptcha.php?q=" + value, true);
        xmlhttp.send();
}

function createCarModel(arg){
    var con = document.getElementById("carModelCon");
    var input = document.createElement("INPUT");

    if(arg.checked){
        input.className = " w3-input w3-border";
        input.name = "carModel";
        input.placeholder = "Enter your car model ...";
        con.appendChild(input);
    }else {
         while(con.firstChild){
            con.removeChild(con.firstChild);      
        }
    }
}

function createLicense(arg){
    var con = document.getElementById("licenseCon");
    var input = document.createElement("INPUT");

    if(arg.checked){
        input.className = " w3-input w3-border";
        input.name = "license";
        input.placeholder = "Enter State of your Driving License ...";
        con.appendChild(input);
    }else {
         while(con.firstChild){
            con.removeChild(con.firstChild);      
        }
    }
}

function createInsurance(arg){
    var con = document.getElementById("insuranceCon");
    var input = document.createElement("INPUT");

    if(arg.checked){
        input.className = " w3-input w3-border";
        input.name = "insurance";
        input.placeholder = "Enter State of your Insurance ...";
        con.appendChild(input);
    }else {
         while(con.firstChild){
            con.removeChild(con.firstChild);      
        }
    }
}

function createPrice(arg){
    var con = document.getElementById("priceCon");
    var input = document.createElement("INPUT");

    if(arg.checked){
        input.className = " w3-input w3-border";
        input.name = "price";
        input.placeholder = "Enter your Fare rate ...";
        con.appendChild(input);
    }else {
         while(con.firstChild){
            con.removeChild(con.firstChild);      
        }
    }
}


function createImg(arg){
    var con = document.getElementById("imgCon");
    var input = document.createElement("INPUT");

    if(arg.checked){
        input.className = " w3-input w3-border";
        input.type = "file";
        input.name = "pic";        
        con.appendChild(input);
    }else {
         while(con.firstChild){
            con.removeChild(con.firstChild);      
        }
    }
}