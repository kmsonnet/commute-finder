# Commute Finder

This project was designed and developed for commuters. If you are a commuter and face problems finding public transports, this conceptual site is for you. 
You will submit your daily route and time. Another commuter of same route and time will find your request and may offer you a ride with some fare. In that way you can both can save money and time.